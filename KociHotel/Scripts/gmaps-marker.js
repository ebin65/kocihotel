﻿          var map;
          $(document).ready(function () {
              map = new GMaps({
                  el: '#map',
                  lat: 51.756493,
                  lng: 19.480276,
              });
              map.addMarker({
                  lat: 51.756493,
                  lng: 19.480276,
                  title: 'Informacja',
                  infoWindow: {
                      content: '<p>Hotel "Koci Raj"<br>ul. Koci Szlak 7<br>91-001 Łódź</p>'
                  }
              });
          });